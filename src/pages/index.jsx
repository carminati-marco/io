import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Parallax } from 'react-spring/renderprops-addons.cjs'

// Components
import Layout from '../components/Layout'
import SkillCard from '../components/SkillCard'

// Elements
import Inner from '../elements/Inner'
import { Title, BigTitle, Subtitle } from '../elements/Titles'

// Views
import Hero from '../views/Hero'
import Skills from '../views/Skills'
import About from '../views/About'
import Contact from '../views/Contact'

import avatar from '../images/avatar.jpg'

const SkillsWrapper = styled.div`
  ${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(2, 1fr);
  @media (max-width: 1200px) {
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`

const AboutHero = styled.div`
  ${tw`flex flex-col lg:flex-row items-center mt-8`};
`

const Avatar = styled.img`
  ${tw`rounded-full w-32 xl:w-48 shadow-lg h-auto`};
`

const AboutDesc = styled.p`
  ${tw`text-grey-light text-lg md:text-xl lg:text-2xl font-sans pt-6 md:pt-12 text-justify`};
`

const AboutSkill = styled.p`
  ${tw`text-grey-light text-sm md:text-lg lg:text-md font-sans pt-6 md:pt-12 text-left`};
`

const ContactText = styled.p`
  ${tw`text-grey-light font-sans text-xl md:text-2xl lg:text-3xl`};
`

const Footer = styled.footer`
  ${tw`text-center text-grey absolute pin-b p-6 font-sans text-md lg:text-lg`};
`

const Index = () => (
  <>
    <Layout />
    <Parallax pages={5}>
      <Hero offset={0}>
        <BigTitle>
          Hi, <br /> I'm Marco.
        </BigTitle>
        <Subtitle>
          <b>Full stack developer</b> | <b>EST. 05</b>
        </Subtitle>
        <Subtitle>
          skilled in{' '}
          <b>
            django - python{' '}
            <span role="img" aria-label="snake">
              🐍
            </span>
          </b>{' '}
          | <b>reactJs - HTML - javascript</b>{' '}
          <span role="img" aria-label="laptop">
            💻{' '}
          </span>
        </Subtitle>
        <Subtitle>
          <b>startup</b> lover{' '}
          <span role="img" aria-label="heart">
            💕
          </span>{' '}
          & <b>pizza</b> addicted{' '}
          <span role="img" aria-label="pizza">
            🍕
          </span>
        </Subtitle>
      </Hero>
      <Skills offset={1}>
        <Title>Skills</Title>
        <SkillsWrapper>
          <SkillCard
            title="Backend"
            link="https://www.behance.net/gallery/58937147/Freiheit"
            bg="linear-gradient(to right, #B43A50 0%, #4F0011 100%)"
          >
            Python, Django, C#, Java, C++*, ExpressJS*, DjangoCMS, Django REST,
            Angular*, flask*
          </SkillCard>
          <SkillCard
            title="Frontend"
            link="https://www.behance.net/gallery/52915793/Harry-Potter"
            bg="linear-gradient(to right, #B43A50 0%, #4F0011 100%)"
          >
            ReactJS, HTML, CSS, Javascript, JQuery, Angular*, ASP.Net
          </SkillCard>
          <SkillCard
            title="DATABASE, SERVICES AND TOOLS"
            link="https://www.behance.net/gallery/43907099/Tomb-Raider"
            bg="linear-gradient(to right, #B43A50 0%, #4F0011 100%)"
          >
            MySQL, MS SQL Server, postgreSQL, Celery, RabbitMQ, ElasticSearch &
            Kibana*, Google Analytics Tag Manager, Git, Gitlab, Plesk, Sentry,
            JIRA, Docker & docker-compose, Google Docs, YAML, Travis, Jenkins*,
            Slack, Confluence, Varnish, Mandrill, Mailchimp, Travis, Redux,
            Jest, Flake8, Gatsby
          </SkillCard>
          <SkillCard
            title="WEB SERVER, OS and OTHERS"
            link="https://www.behance.net/gallery/38068151/Eagle"
            bg="linear-gradient(to right, #B43A50 0%, #4F0011 100%)"
          >
            Apache, nginx, gunicorn, lighttpd*, JBoss*, WebSphere*, IIS*
            Ubuntu/Linux, MS Windows, macOS, MS Office Package, LibreOffice,
            Pages, Keynote, Numbers, Wordpress
          </SkillCard>
        </SkillsWrapper>
        <AboutSkill>
          * indicates that it was used years ago or is under study
        </AboutSkill>
      </Skills>
      <About offset={3}>
        <Title>About</Title>
        <AboutHero>
          <Avatar src={avatar} alt="Marco Carminati" />
        </AboutHero>
        <AboutDesc>
          Working between London and Milan, I have several experiences in{' '}
          <b>Django</b>, <b>ReactJS</b>, <b>REST+GraphQL</b>, <b>Agile Scrum</b>
          , <b>UML</b>, <b>version control</b>, <b>CI/CD &amp; TDD</b>; these
          are the items used in my latest projects.
          <br />
          Would you like to know more? Let's get in touch!
        </AboutDesc>
      </About>
      <Contact offset={4}>
        <Inner>
          <Title>Is it a match?</Title>
          <ContactText>
            <a href="mailto:carminati.marco@gmail.com">Text me</a> or use other
            platforms:{' '}
            <a href="https://www.linkedin.com/in/marco-carminati/">linkedin</a>,{' '}
            <a href="https://twitter.com/carminatimarco">twitter</a> &amp;{' '}
            <a href="https://gitlab.com/carminati-marco/">gitlab</a>
          </ContactText>
        </Inner>
        <Footer>
          Thanks to <a href="https://www.lekoarts.de">LekoArts</a> for the
          starter, <a href="https://www.gatsbyjs.org/">Gatsby</a> &{' '}
          <a href="https://reactjs.org/">ReactJs</a>.
        </Footer>
      </Contact>
    </Parallax>
  </>
)

export default Index
