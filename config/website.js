const tailwind = require("../tailwind");

module.exports = {
  pathPrefix: "/io", // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "/portfolio"

  siteTitle: "Marco Carminati | Full stack developer | EST 05", // Navigation and Site Title
  siteTitleAlt: "Marco Carminati", // Alternative Site title for SEO
  siteTitleShort: "Marco Carminati", // short_name for manifest
  siteHeadline: "Skilled in Django/Python + ReactJS", // Headline for schema.org JSONLD
  siteUrl: "https://carminati-marco.gitlab.io", // Domain of your site. No trailing slash!
  siteLanguage: "en", // Language Tag on <html> element
  siteLogo: "/logo.png", // Used for SEO and manifest
  siteDescription: "Skilled in Django/Python + ReactJS",
  author: "MarcoCarminati", // Author for schema.org JSONLD

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: "@carminatimarco", // Twitter Username
  ogLanguage: "en_US", // Facebook Language
  googleAnalyticsID: "UA-131541330-1",

  // Manifest and Progress color
  themeColor: tailwind.colors["blue-light"],
  backgroundColor: tailwind.colors.blue
};
